# StatusPage.io Banners for Atlassian Server products

Works across all server products!
Bamboo, JIRA, Confluence, Fisheye, Bitbucket

- Two display styles to choose from:
 - Displays a banner at top of page for current status from the specified statuspage.io page.
    (Optionally banner can be disabled entirely for healthy status)
 - Display more detailed listings as Nav Menu Drop Down
    (Allows filtering by component and shows scheduled maintenance)
- Displays impact colors, status icons, summary and links to StatusPage.io details.
- Includes JIRA Service Desk banner with similar style to built in announcements

![Incident example in bamboo](src/main/resources/images/newnavstyle.png)
![Incident example in bamboo](src/main/resources/images/legacystyle.png)


# Configuration

1. Go to   Add-Ons > Manage in the Admin Menu
![Manage Add-ONs](src/main/resources/images/addons.png)
2. Find & expand StatusPage Banner for Atlassian Server from the listed plugins
![Manage Add-ONs](src/main/resources/images/addons_configure.png)
3. Click the Configure button in that panel
![Configure StatusPage.io integration with PageID](src/main/resources/images/configure.png)
4. Provide the StatusPage.io page ID, enable and save!

# More Pictures

These are shown using the Legacy Style banner

### JIRA Support
![StatusPage.io integration example in jira](src/main/resources/images/jira_minor_with_incident.png)

### Bitbucket Support
![StatusPage.io integration  example in Bitbucket](src/main/resources/images/bb_minor_with_incident.png)

### Confluence
![StatusPage Incident example in confluence](src/main/resources/images/confluence_healthy_visible.png)
(Note: When summary indicator is 'none' as show above, the banner slides up in ~3 seconds)





# Developing

## Products
To specify product for tests or running locally, use testGroups
`atlas-run --testGroup jira`
`atlas-mvn clean test verify --testGroup bamboo`

- bamboo
- bitbucket
- jira
- confluence
- fecru
- refapp

## StatusPge testing
Use 39qnmk3k9q7f which is sample page from statuspage.io.  It seems to cycle from healthy to outages at some interval.
