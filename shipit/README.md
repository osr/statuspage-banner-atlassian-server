This file provides settings and customizations for the shipit images used to publish to marketplace.

See https://hub.docker.com/r/eddiewebb/bitbucket-pipelines-marketplace/ or https://bitbucket.org/eddiewebb/bitbucket-pipelines-marketplace-docker