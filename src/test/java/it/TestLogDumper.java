package it;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Created by Eddie Webb on 3/1/17.
 */
public class TestLogDumper extends TestWatcher{

    private final StringBuilder debugLogOrInfo = new StringBuilder();

    public void append(String string){
        debugLogOrInfo.append(string);
        debugLogOrInfo.append("\n");
    }
    @Override
    protected void starting(Description description) {
        debugLogOrInfo.setLength(0);
    }

    @Override
    protected void failed(Throwable e, Description description) {
        System.err.print(debugLogOrInfo.toString());
    }

    @Override
    protected void succeeded(Description description) {
        super.succeeded(description);
    }

    @Override
    protected void finished(Description description) {
        super.finished(description);
    }
}
