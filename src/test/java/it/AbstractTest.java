package it;

import com.edwardawebb.atlassian.statuspage.impl.ConfigResource;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.RestClient;
import org.apache.wink.client.handlers.BasicAuthSecurityHandler;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public abstract class AbstractTest {
    public static final String BASE_URL =  System.getProperty("baseurl", "http://localhost:5990/product");
    public static final String HTTP_PORT =  System.getProperty("http.port", "5990");
    public static final String CONTEXT_PATH =  System.getProperty("context.path", "/product");
    public static final String REST_URL = BASE_URL + "/rest/statuspagebanner/1.0/";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    protected ConfigResource.Config adminConfig = new ConfigResource.Config();
    @Rule public TestLogDumper testLogDumper = new TestLogDumper();
    @Rule public TestName name = new TestName();
    protected RestClient client;
    private ClientConfig clientConfig;
    protected RemoteWebDriver driver;

    // Enable Sauce Labs browser testing, compliment Open Source license.  SauceLabs.com
    private static String SAUCE_USER = System.getenv("sauceUser");
    private static String SAUCE_ACCESS_KEY = System.getenv("sauceApiKey");
    public static final String SAUCE_URL = "http://" + SAUCE_USER + ":" + SAUCE_ACCESS_KEY + "@localhost:4445/wd/hub";

    // SP testing objects
    public static final String PAGE_ID = "39qnmk3k9q7f";
    public static final String MOCK_PAGE_ID = "MOCKSTATUSPAGEID";
    public static final String MOCK_SP_STATUS = "Minor Service Outage";
    public static final String SP_PAGE_NAME = "Acme API Co.";
    public static final String SP_MOCKED_PAGE_NAME = "MOCKED Acme API Co.";
    public static final String COMPONENTS = "Bamboo - Build Farm";


    //Apache Wink still used to interact with REST service, supports object marshalling
    protected void enableStatusPage(String components, String pageId){
        javax.ws.rs.core.Application app = new javax.ws.rs.core.Application() {
            public Set<Class<?>> getClasses() {
                Set<Class<?>> classes = new HashSet<Class<?>>();
                classes.add(JacksonJaxbJsonProvider.class);
                return classes;
            }
        };
        //create auth handler
        clientConfig = new ClientConfig();
        clientConfig.setLoadWinkApplications(false);
        clientConfig.applications(app);
        BasicAuthSecurityHandler basicAuthSecurityHandler = new BasicAuthSecurityHandler();
        basicAuthSecurityHandler.setUserName(USERNAME);
        basicAuthSecurityHandler.setPassword(PASSWORD);
        clientConfig.handlers(basicAuthSecurityHandler);
        //create client usin auth
        client = new RestClient(clientConfig);
        adminConfig.setEnabled(true);
        adminConfig.setPageId(pageId);
        adminConfig.setShowHealthy(true);
        adminConfig.setComponents(components);
        System.out.println("==== Calling URL "+ REST_URL + " to enable SP.");
        ClientResponse response = client.resource(REST_URL).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).put(adminConfig);
        System.out.println("message: "+ response.getMessage());
        assertThat("Could not enable sp for test",response.getStatusCode(), anyOf(is(200),is(204)));
    }


    public void createDriver() throws MalformedURLException{
        DesiredCapabilities caps = DesiredCapabilities.chrome();
        caps.setCapability("platform", "Windows 10");
        caps.setCapability("version", "57.0");
        caps.setCapability("name", generateTestName(name.getMethodName()));
        caps.setCapability("build",generateBuildInfo());
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.INFO);
        caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        System.out.println("Testing against BaseURL: " + BASE_URL);
        driver = new RemoteWebDriver(new URL(SAUCE_URL), caps);
    }



    private String generateBuildInfo() {
        // see https://confluence.atlassian.com/bitbucket/environment-variables-794502608.html
        String gitCommit = System.getenv("BITBUCKET_COMMIT");
        if( null == gitCommit ){
            gitCommit = "NON CI BUILD";
        }
        return gitCommit;
    }


    private String generateTestName(String testName) {
        String product = System.getProperty("testGroup","RefApp");
        String version = System.getProperty(product + ".version");
        return testName + "In" + product + "-" + version;
    }

}
