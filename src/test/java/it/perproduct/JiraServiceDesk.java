package it.perproduct;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import it.AbstractTest;
import org.junit.*;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Test component imports and exports of the test plugin when actually installed in the product.
 */
public class JiraServiceDesk extends AbstractTest
{


    /**
     * This only tests that products expose the parent {@code <div>} we append to.
     * It does not validate JS execution or functionality
     * @throws IOException
     */
/*    @Test
    @Ignore("JSD js used to load content asynchronously fails in htmlunit, need alternate approach")
    public void testParentElementsArePresentInDom() throws IOException {
        final WebClient webClient = new WebClientBuilder().withBasicAuthHeader().thatFollowsRedirects().build();
        final HtmlPage page = webClient.getPage(BASE_URL + "/servicedesk/customer/portals");
        //Even basic page contents is asynchronous in JSD
        webClient.waitForBackgroundJavaScript(2 * 1000);
        System.out.print("=======" + page.asXml());
        List<DomElement> elements = page.getByXPath("//div[@class='cv-portal-announcement']");
        assertThat("Unable to parent dom element" ,elements.size(), is(1));
    }

    *//**
     * This test allows our JS to execute and validates the manipulated dom
     * @throws IOException
     *//*
    @Test
    @Ignore("JSD js used to load content asynchronously fails in htmlunit, need alternate approach")
    public void testDomIsModifiedByJs() throws IOException {
        enableStatusPage();
        final WebClient webClient = new WebClientBuilder().withBasicAuthHeader().thatFollowsRedirects().build();
        final HtmlPage page = webClient.getPage(BASE_URL + "/servicedesk/customer/portals");
        webClient.waitForBackgroundJavaScript(5 * 1000);
        String manipulatedDom = page.asXml();
        assertTrue("Unable to find rendered SP contents" ,manipulatedDom.contains("sp-jsd-announcement"));
    }*/


}