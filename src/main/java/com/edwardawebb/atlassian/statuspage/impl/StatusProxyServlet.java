package com.edwardawebb.atlassian.statuspage.impl;

import com.edwardawebb.atlassian.statuspage.api.StatusPageSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

@Component
public class StatusProxyServlet extends HttpServlet
{
    private final StatusPageSettings statusPageSettings;
    private static final Logger log = LoggerFactory.getLogger(StatusProxyServlet.class);
    private static final String STATUS_PAGE_URL_FORMAT = "https://%s.statuspage.io/api/v2/summary.json";
    private static final String STATUS_PAGE_MOCK_PAGE_ID = "MOCKSTATUSPAGEID";

    @Autowired
    public StatusProxyServlet(final StatusPageSettings statusPageSettings)
    {
        this.statusPageSettings = statusPageSettings;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        boolean returnMockPayload = (
                    request.getParameterMap().containsKey("mock") || STATUS_PAGE_MOCK_PAGE_ID.equals(statusPageSettings.getPageId())
                );
        response.setContentType("application/json");
        if( ! returnMockPayload ) {
            URL spTargetUrl = new URL(String.format(STATUS_PAGE_URL_FORMAT, statusPageSettings.getPageId()));
            //TODO: change all warn to info/debug
            log.debug("Calling SP URL: {}",spTargetUrl.toString());
            HttpURLConnection connection;
            if(null != System.getProperty("http.proxyHost")){
                String proxyHost = System.getProperty("http.proxyHost");
                int proxyPort = Integer.parseInt(System.getProperty("http.proxyPort","80"));
                Proxy proxy = new Proxy(Proxy.Type.HTTP,new InetSocketAddress(proxyHost,proxyPort));
                connection = (HttpURLConnection)spTargetUrl.openConnection(proxy);
                log.debug("Using proxy: {}:{}",proxyHost,proxyPort);
            }else{
                connection = (HttpURLConnection)spTargetUrl.openConnection();
                log.debug("No proxy");
            }
            addApiKeyIfSpecified(connection);
            connection.connect();
            log.debug("Using Proxy? {}",connection.usingProxy());
            log.debug("Connection status: {}", connection.getResponseCode());
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String spResponse;
            while ((spResponse = in.readLine()) != null){
                response.getWriter().write(spResponse);
            }
            in.close();
        }else{
            String mockJsonFilename = "data/dummy_status.json";
            ClassPathResource resource = new ClassPathResource(mockJsonFilename);
            try {
                String bodyToReturn = FileCopyUtils.copyToString(new InputStreamReader(resource.getInputStream()));
                response.getWriter().print(bodyToReturn);
            } catch (IOException e) {
                throw new RuntimeException("MOCK RESPONSE ERROR: unable to find mock data file with name " + mockJsonFilename, e);
            }
        }
    }

    private void addApiKeyIfSpecified(HttpURLConnection connection) {
        if(null != statusPageSettings.getApiKey()){
            connection.setRequestProperty("Authorization","OAuth " + statusPageSettings.getApiKey());
        }
    }


}