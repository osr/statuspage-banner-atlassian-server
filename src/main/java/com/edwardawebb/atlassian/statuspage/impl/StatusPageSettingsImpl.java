package com.edwardawebb.atlassian.statuspage.impl;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.edwardawebb.atlassian.statuspage.api.StatusPageSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Named;
import java.util.List;

/**
 * Gateway to PluginSettings using appropriate keys
 */
@Component
public class StatusPageSettingsImpl implements StatusPageSettings {

    public static final String STATUS_PAGE_ID = "com.edwardawebb.statuspage.id";
    public static final String STATUS_PAGE_ON = "com.edwardawebb.statuspage.enabled";
    public static final String STATUS_PAGE_LEGACY = "com.edwardawebb.statuspage.legacy";
    public static final String STATUS_PAGE_ON_HEALTHY = "com.edwardawebb.statuspage.showhealthy";
    public static final String STATUS_PAGE_COMPONENTS = "com.edwardawebb.statuspage.components";
    public static final String STATUS_PAGE_API_KEY = "com.edwardawebb.statuspage.apiKey";
    public static final String SERVICEDESK_PORTAL_ID = "com.edwardawebb.statuspage.serviceDeskId" ;

    private String pageId;
    private boolean isEnabled;

    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public StatusPageSettingsImpl(@ComponentImport PluginSettingsFactory pluginSettingsFactory, @ComponentImport TransactionTemplate transactionTemplate){
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
    }

    public void setPageId(String pageId) {
        storeSetting(STATUS_PAGE_ID,pageId);
    }

    public String getPageId() {
        return accessSetting(STATUS_PAGE_ID);
    }

    @Override
    public String getApiKey() {
        return accessSetting(STATUS_PAGE_API_KEY);
    }

    @Override
    public void setApiKey(String apiKey) {
        storeSetting(STATUS_PAGE_API_KEY,apiKey);
    }

    @Override
    public String getComponents() {
        return accessSetting(STATUS_PAGE_COMPONENTS);
    }

    @Override
    public void setComponents(String components) {
        storeSetting(STATUS_PAGE_COMPONENTS,components);
    }

    public void setEnabled(boolean isEnabled) {
        storeSetting(STATUS_PAGE_ON,Boolean.toString(isEnabled));
    }

    public boolean isEnabled() {
        String enabled = accessSetting(STATUS_PAGE_ON);
        if ( null != enabled ){
            return Boolean.parseBoolean(enabled);
        } else {
            return false;
        }
    }

    public void setLegacyStyle(boolean isLegacyStyle) {
        storeSetting(STATUS_PAGE_LEGACY,Boolean.toString(isLegacyStyle));
    }

    public boolean isLegacyStyle() {
        String isLegacyStyle = accessSetting(STATUS_PAGE_LEGACY);
        if ( null != isLegacyStyle ){
            return Boolean.parseBoolean(isLegacyStyle);
        } else {
            return false;
        }
    }

    public void setShowHealthy(boolean isShowHealthy) {
        storeSetting(STATUS_PAGE_ON_HEALTHY,Boolean.toString(isShowHealthy));
    }

    public boolean isShowHealthy() {
        String isShowHealthy = accessSetting(STATUS_PAGE_ON_HEALTHY);
        if ( null != isShowHealthy ){
            return Boolean.parseBoolean(isShowHealthy);
        } else {
            return false;
        }
    }

    @Override
    public void setServiceDeskPortal(int serviceDeskPortal) {
        storeSetting(SERVICEDESK_PORTAL_ID,String.valueOf(serviceDeskPortal));
    }

    @Override
    public int getServiceDeskPortal() {
        String serviceDeskId = accessSetting(SERVICEDESK_PORTAL_ID);
        if( null != serviceDeskId ){
            return Integer.parseInt(serviceDeskId);
        }else{
            return 0;
        }

    }

    private String accessSetting(final String key) {
        return (String) transactionTemplate.execute(new TransactionCallback()
        {
            public String doInTransaction()
            {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                return (String) settings.get(key);
            }
        });
    }

    private void storeSetting(final String key,final String value) {
        transactionTemplate.execute(new TransactionCallback()
        {
            public Object doInTransaction()
            {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                settings.put(key,value);
                return null;
            }
        });
    }
}
