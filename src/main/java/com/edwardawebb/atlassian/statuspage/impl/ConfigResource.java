package com.edwardawebb.atlassian.statuspage.impl;


import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import com.edwardawebb.atlassian.statuspage.api.StatusPageSettings;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Path("/")
public class ConfigResource
{

    private final UserManager userManager;
    private final StatusPageSettings statusPageSettings;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public ConfigResource(UserManager userManager, StatusPageSettings statusPageSettings,
                          TransactionTemplate transactionTemplate)
    {
        this.userManager = userManager;
        this.statusPageSettings = statusPageSettings;
        this.transactionTemplate = transactionTemplate;
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    public static final class Config
    {
        @XmlElement
        private String pageId;
        @XmlElement
        private String apiKey;
        @XmlElement
        private boolean enabled;
        @XmlElement
        private boolean legacyStyle;
        @XmlElement
        private boolean showHealthy;
        @XmlElement
        private String components;
        @XmlElement
        private int serviceDeskId;

        public String getPageId() {
            return pageId;
        }

        public Config setPageId(String pageId) {
            this.pageId = pageId;
            return this;
        }

        public String getApiKey() {
            return apiKey;
        }

        public Config setApiKey(String apiKey) {
            this.apiKey = apiKey;
            return this;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public Config setEnabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public boolean isLegacyStyle() {
            return legacyStyle;
        }

        public Config setLegacyStyle(boolean legacyStyle) {
            this.legacyStyle = legacyStyle;
            return this;
        }

        public boolean isShowHealthy() {
            return showHealthy;
        }

        public Config setShowHealthy(boolean showHealthy) {
            this.showHealthy = showHealthy;
            return this;
        }

        public String getComponents() {
            return components;
        }

        public Config setComponents(String components) {
            this.components = components;
            return this;
        }

        public int getServiceDeskId() {
            return serviceDeskId;
        }

        public Config setServiceDeskId(int serviceDeskId) {
            this.serviceDeskId = serviceDeskId;
            return this;
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request)
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username))
        {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        return Response.ok( buildConfigResource() ).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(final Config config, @Context HttpServletRequest request)
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username))
        {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        if(config.isEnabled() && StringUtils.isEmpty(config.getPageId())){
            return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON_TYPE).entity("{\"message\":\"Must provide pageId if enabling banner\"}").build();
        }
        statusPageSettings.setEnabled(config.isEnabled());
        statusPageSettings.setPageId(config.getPageId());
        statusPageSettings.setApiKey(config.getApiKey());
        statusPageSettings.setShowHealthy(config.isShowHealthy());
        statusPageSettings.setComponents(config.getComponents());
        statusPageSettings.setServiceDeskPortal(config.getServiceDeskId());
        statusPageSettings.setLegacyStyle(config.isLegacyStyle());
        if(config.isLegacyStyle()){
            statusPageSettings.setShowHealthy(false);
        }
        return Response.noContent().build();
    }


    /**
     * Expose endpoint JS can use to load details to replace velocity context dependency
     * @param request
     * @return
     */
    @GET
    @Path("public")
    @Produces(MediaType.APPLICATION_JSON)
    @AnonymousAllowed
    public Response getPublic(@Context HttpServletRequest request)
    {
        Config config = buildConfigResource();
        config.setApiKey(null);
        return Response.ok( config ).build();
    }



    private Config buildConfigResource(){
        Config config = new Config();
        config.setPageId(statusPageSettings.getPageId());
        config.setApiKey(statusPageSettings.getApiKey());
        config.setEnabled(statusPageSettings.isEnabled());
        config.setShowHealthy(statusPageSettings.isShowHealthy());
        config.setServiceDeskId(statusPageSettings.getServiceDeskPortal());
        config.setComponents(statusPageSettings.getComponents());
        config.setLegacyStyle(statusPageSettings.isLegacyStyle());
        return config;
    }

}
