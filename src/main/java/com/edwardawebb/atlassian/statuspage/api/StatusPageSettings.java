package com.edwardawebb.atlassian.statuspage.api;

import java.util.List;

/**
 * Interface to Atlassian PluginSettingsService with proper keys
 */
public interface StatusPageSettings {
    void setPageId(String pageId);
    String getPageId();

    String getApiKey();
    void setApiKey(String apiKey);

    String getComponents();
    void setComponents(String components);

    void setEnabled(boolean isEnabled);
    boolean isEnabled();

    void setLegacyStyle(boolean isLegacyStyle);
    boolean isLegacyStyle();

    void setShowHealthy(boolean isShowHealthy);
    boolean isShowHealthy();

    void setServiceDeskPortal(int serviceDeskPortal);
    int getServiceDeskPortal();
}
