/***
**  Product specific file has 2 functions:
**  1) Define handler named displayStatusPage call to render summary in UI once config is set
**  2) Call launch() once AJS is ready (most products just immediately invoke)
**
**/

// let our stuff fail without breaking product UI using IIFE concept
function displayStatusPage(summary){
    if( statuspageConfig.legacyStyleBanner ){
        console.log("SHowing legacy banner format");
        var legacyBanner = Edwardawebb.Statuspage.Banner.bannerBar({summary: summary, config:statuspageConfig});
        if( AJS.$('#sp-banner').length ){
            AJS.$('#sp-banner').replaceWith(legacyBanner);
        }else{
            AJS.$('body').prepend(legacyBanner);
        }
    }else{
        //adam style (thanks to Adam Al-Ibraham for the formatting idea!)
        var navLink = Edwardawebb.Statuspage.Banner.navLink({summary: summary, config:statuspageConfig});
        if( AJS.$('#sp-navlink').length ){
            console.log("async change, update status");
            AJS.$('#sp-navlink').replaceWith(navLink);
        }else{
            console.log("inserting");
            AJS.$('div.aui-header-primary ul.aui-nav').before(navLink);
        }
    }
}

// This file is for standard products, AJS was declared as dependecny and immediately ready.
launch();