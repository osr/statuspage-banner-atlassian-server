/******
**   This file is called by product specific files.
** It's job is to:
** 1) Discover baseURl and load StatusPage config from backend product
** 2) Callback to product specific displayStatusPage()
** 3) monitor pages for AJAX url changes and re-inject status page
**/


// global variabled used by product specific display pages.
var statuspageConfig;

//JSD (and a few random JIRA pages) uses ajax to juimp between pages, make sure our magic happens
var currentPage = window.location.href;

function fromProductContext(contextPath){
    return window.location['protocol'] + "//" + window.location['host'] + contextPath;
}

// Called by product page loaded after this file
function launch(){
    AJS.toInit(function() {
        var baseUrl;
        if( AJS.params.baseURL ){
            baseUrl = AJS.params.baseURL;
        }else if (AJS.contextPath ){
            baseUrl = fromProductContext(AJS.contextPath());
            console.log("Constructed baseUrl from window.location and AJS.contextPath:" + baseUrl);
        }else if( AJS.$("meta[name='application-base-url']").length ){
            //most products stash url in a data attribute
            baseUrl = AJS.$("meta[name='application-base-url']").attr("content");
        }else{
            console.log("Unable to find baseUrl, statuspage will not work");
            return;
        }
        AJS.$.ajax({
              url: baseUrl + "/rest/statuspagebanner/1.0/public",
              dataType: "json",
              success: function(config) {
                  console.log({"message":"Success getting SP config","payload":config});
                  statuspageConfig = config;
                  if(config.enabled){
                        //query and display status
                        queryStatus(baseUrl);
                        // listen for URL changes, requery status and redisplay
                        setInterval(function(){
                            if (currentPage != window.location.href)
                            {
                                currentPage = window.location.href;
                                queryStatus(baseUrl);
                            }
                        }, 100);
                  }else{
                    console.log("SP disabled by settings");
                  }
              },
               error: function(jqXHR, textStatus, errorThrown){
                  console.log("SP call errored with status " + textStatus);
                  console.log(errorThrown);
                  console.log(jqXHR);
                }
        });
    });
}


function queryStatus(baseUrl){
    if(statuspageConfig && statuspageConfig.pageId){
        console.log("Status Page Banner JS Loading...");
        AJS.$.ajax({
              url: baseUrl + "/plugins/servlet/statusproxy",
              dataType: "json",
              success: function(data) {
                  console.log({"SP Response":data});
                  var summary = modifyStatusPageJs(JSON.parse(JSON.stringify(data)));
                  displayStatusPage(summary);
              },
              error: function(jqXHR, textStatus, errorThrown){
                 console.log("SP call errored with status " + textStatus);
                 console.log(errorThrown);
                 console.log(jqXHR);
               }
        });
    }else{
        console.log("StatusPage banner ID missing or disabled");
    }
}


