AJS.toInit(function() {
  var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");

  function populateForm() {
    AJS.$.ajax({
      url: baseUrl + "/rest/statuspagebanner/1.0/",
      dataType: "json",
      success: function(config) {
        AJS.$("#pageId").attr("value", config.pageId);
        AJS.$("#apiKey").attr("value", config.apiKey);
        AJS.$("#components").attr("value", config.components);
        AJS.$("#serviceDeskId").attr("value", config.serviceDeskId);
        AJS.$("#enabled").attr("checked", config.enabled);
        AJS.$("#showHealthy").attr("checked", config.showHealthy);
        AJS.$("#legacyStyle").attr("checked", config.legacyStyle);
      }
    });
  }
  function updateConfig() {
    var data = { "enabled": AJS.$("#enabled").attr("checked")?true:false ,"legacyStyle": AJS.$("#legacyStyle").attr("checked")?true:false ,"showHealthy": AJS.$("#showHealthy").attr("checked")?true:false , "pageId": AJS.$("#pageId").attr("value"), "apiKey": AJS.$("#apiKey").attr("value"), "components": AJS.$("#components").attr("value"), "serviceDeskId": AJS.$("#serviceDeskId").attr("value") };
    console.log("Submitting updated form data to backend");
    console.log(data);
    AJS.$.ajax({
      url: baseUrl + "/rest/statuspagebanner/1.0/",
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(data),
      processData: false,
      success: function(){
            console.log("success");
            AJS.messages.success({
                title: "Saved!",
                body: "Config updated."
             });
      },
      error: function(xhr, status, error) {
            console.log({status: status,response:xhr});
            var err = xhr.responseJSON?xhr.responseJSON.message:xhr.responseText;
            AJS.messages.error({
                title: "Error!",
                body: err
             });
      }
    });
  }
  populateForm();

  AJS.$("#sp-admin").submit(function(e) {
    e.preventDefault();
    updateConfig();
  });
});