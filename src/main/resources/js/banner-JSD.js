/***
**  Product specific file has 2 functions:
**  1) Call launch() once AJS is ready (most products just immediately invoke)
**  2) Handle resultsing displayStatusPage call to render summary in UI
**
**/
// let our stuff fail without breaking product UI using IIFE concept
function displayStatusPage(summary){
    if( statuspageConfig.serviceDeskId > 0){
        // URL is update ddynamically wih ajax, check for portal ID.
        try{
            var portalId = /.*\/portal\/([0-9]*).*/.exec(window.location.href)[1]
        }catch(err){
            console.log("Not in any specific portal, hide banner");
            return;
        }
        console.log("Checking if JSD Portal " + portalId + " should show status..");
        if ( !portalId || statuspageConfig.serviceDeskId != portalId ){
            console.log("Admin settings disabled StatusPage for this Service Desk");
            return;
        }
    }
    if( statuspageConfig.legacyStyle ){
        console.log("SHowing legacy banner format");
        //adam style (thanks to Adam Al-Ibraham for the formatting idea!)
        var legacyBanner = Edwardawebb.Statuspage.Banner.bannerBar({summary: summary, config:statuspageConfig});
        AJS.$('body').prepend(legacyBanner);
    }else{
        if(AJS.$('div.cv-portal-announcement').length > 0){
            //main JSD landing page
            console.log("showing JSD ANnouncement");
            var brief = Edwardawebb.Statuspage.Banner.summary({summary: summary, config:statuspageConfig});
            var jsdmessage = Edwardawebb.Statuspage.Banner.jsdmessage({summary: summary, config:statuspageConfig});
            AJS.$('div.announcement-header').html(summary.page.name + " Status: "  + brief);
            AJS.$('div.announcement-message').html(jsdmessage);
            AJS.$('div.cv-portal-announcement').attr( "style", "display: block !important;" );
        }else{
            //specific portals
            console.log("showing JSD navlink");
            var navlink = Edwardawebb.Statuspage.Banner.navlink({summary: summary, config:statuspageConfig});
            AJS.$('div.aui-header-primary').append(navlink);
        }
    }
}



// JSD errors if you declare AJS dependency, but it is available, but may load after our code
var ajsAttempt=0;
function onFunctionAvailable() {
    if(ajsAttempt++ > 50){
        console.log("AJS is not available. Statuspage integration will not load.");
        return;
    }
    try{
        AJS.$.length
        console.log("AJS Methods Ready");
        launch();
	}catch(err){
	    console.log(err);
	    console.log("AJS Methods not ready");
	    setTimeout(function () {
           		onFunctionAvailable();
           		}, 50);
	}
}
onFunctionAvailable();





